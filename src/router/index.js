import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

const routes = [
    {
        path: '/',
        name: 'Login',
        component: () => import(/* webpackChunkName: "login"*/'../components/StartScreen/Login')
    },
    {
        path: '/triviaconfig',
        name: 'TriviaConfig',
        component: () => import(/* webpackChunkName: "triviaconfig"*/'../components/StartScreen/TriviaConfig')
    },
    {
        path: '/trivia',
        name: 'Trivia',
        component: () => import(/* webpackChunkName: "trivia"*/'../components/QuestionScreen/Trivia')
    },
    {
        path: '/result',
        name: 'Result',
        component: () => import(/* webpackChunkName: "result"*/'../components/ResultScreen/Result')
    }
]

const router = new VueRouter({
    mode: 'history',
    base: process.env.BASE_URL,
    routes
})

export default router;