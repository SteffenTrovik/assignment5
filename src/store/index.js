import Vue from 'vue'
import Vuex from 'vuex'
import{LoginAPI} from "../components/StartScreen/LoginAPI";
import {QuestionsAPI} from "../components/StartScreen/QuestionsAPI";
import {TriviaAPI} from "../components/QuestionScreen/TriviaAPI.JS";


Vue.use(Vuex)

export default new Vuex.Store({
    state: {
        //user
        username: '',
        highScore: '0',
        Id: '',
        token: '',

        //trivia options
        questionAmountAvaliable: '',
        categories: {},

        //trivia selections
        difficulty: '',
        category:'',
        questionAmount:10,
        questionType:'',

        //active trivia questions
        questionIndex: 0,
        questionText: '',
        questionAlternatives: [],
        loadingQuestions: true,
        questions: {

        },
        answer: '',
        score:0,

        //error
        error: '',

    },
    mutations: {
        //user
        setUsername: (state, payload) => {
            state.username = payload
        },
        setHighScore: (state, payload) => {
            state.HighScore = payload
        },
        setId: (state,payload) => {
            state.Id=payload
        },
        setToken: (state, payload) => {
            state.token = payload
        },
        //trivia options
        setQuestionAmountAvaliable: (state, payload) => {
            state.QuestionAmountAvaliable = payload
        },
        setCategories: (state, payload) => {
            state.categories = payload
        },
        //trivia selections
        setDifficulty: (state, payload) => {
            state.difficulty = payload
        },
        setCategory: (state, payload) => {
            state.category = payload
        },
        setQuestionAmount: (state, payload) => {
            console.log("updating qamount")
            state.questionAmount = payload
        },
        setQuestionType: (state, payload) => {
            state.questionType = payload
        },
        //active trivia
        setQuestionIndex: (state,payload) =>{
            state.questionIndex = payload
        },
        setQuestionText: (state,payload) =>{
            state.questionText = payload
        },
        setQuestionAlternatives:(state,payload)=>{
            state.questionAlternatives = payload
        },
        setLoadingQuestions: (state, payload) => {
            state.loadingCharacters = payload
        },
        setQuestions: (state, payload) => {
            state.questions = payload
        },
        setAnswer: (state, payload) => {
            state.answer = payload
        },
        setScore: (state, payload) => {
            state.score=payload
        },
        setError: (state, payload) => {
            state.error = payload
        }
    },
    getters: {
        allCategories: state =>{
            return state.categories
        },
        getQuestionAmount: state =>{
            return state.questionAmount;
        }
    },
    actions: {
        //Logs in/registers user using fetches in the LoginAPI
        async loginUser({state, commit}){
            try {
                const loginUsername = state.username
                const user = await LoginAPI.login(loginUsername)
                state.highScore=user.highScore
                state.Id=user.Id
                commit('setUsername',user.username)
                commit('setHighScore',user.highScore)
                commit('setId',user.id)
            }catch (e) {
                commit('setError',e.message)
            }
        },

        // grabs a token for non repeat questions using questionapi
        async fetchToken({commit}){ //maybe state here
            try {
                const token = await QuestionsAPI.fetchToken()
                commit('setToken',token)
            }catch (e) {
                commit('setError',e.message)

            }
        },

        // grabs the categories avaliable to display on triviaconfig using questionapi
        async fetchCategories({commit}){ // maybe state here
            try {
                const categories = await QuestionsAPI.fetchCategories()
                commit('setCategories',categories)
            }catch (e) {
                commit('setError',e.message)
            }
        },

        //grabs the amount of questions in a given category using questionapi
        async fetchQuestionAmountAvaliable({commit,state}) {
            try {
                const category = state.category
                const questionAmountAvaliable = await QuestionsAPI.fetchQuestionAmountInCategory(category)
                commit('setQuestionAmountAvaliable', questionAmountAvaliable)
            } catch (e) {
                commit('setError', e.message)
            }
        },

        //loads first question in triva after you click start
        async displayNextQuestion({commit,state}){
            try{
                let questionText=state.questions[state.questionIndex].question
                state.questionText=questionText
                let questionAlternatives=TriviaAPI.loadNextQuestion(state.questions,state.questionIndex)

                state.loadingQuestions=false;
                commit('setLoadingQuestions', false)
                commit('setQuestionText',questionText)
                commit('setQuestionAlternatives',questionAlternatives)

            }catch(e){
                commit('setError',e.message)
            }
        },

        //grabs the questions given the triviaconfig, uses questionapi
        async fetchQuestions({commit,state}){
            try {
                state.questionIndex=0
                state.score=0
                state.questionAlternatives=[]
                const amount=state.questionAmount
                const category=state.category
                const difficulty = state.difficulty
                const questionType = state.questionType
                const token =state.token
                const questions = await QuestionsAPI.fetchQuestions(amount,category,difficulty,questionType,token)
                commit('setQuestions',questions)
                //await this.displayNextQuestion()
            }catch (e) {
                commit('setError',e.message)

            }
        },

        //sets new highscore to user if its higher than old highscore, uses triviaapi
        async newHighScore({commit,state}){
            try {
                if(state.score>state.highScore) {
                    state.highScore = state.score
                    await TriviaAPI.setNewHighScore(state.Id,state.score)
                }
                commit('setHighScore',state.score)
            } catch (e) {
                commit('setError', e.message)
            }
        },

        //answers a question in trivia and loads the next one
        //sets question in random order
        async answerQ({commit, state}) {
            try {
                if(state.questions[state.questionIndex].correct_answer==state.answer)
                {
                    state.score+=10
                }
                state.questions[state.questionIndex].answered=state.answer
                state.questionIndex++
                let questionText=state.questions[state.questionIndex].question
                state.questionText=questionText
                let questionAlternatives=TriviaAPI.loadNextQuestion(state.questions,state.questionIndex)

                commit('setQuestionText',questionText)
                commit('setQuestionAlternatives',questionAlternatives)
            } catch (e) {
                commit('setError', e.message)
            }
        }
    }
})










