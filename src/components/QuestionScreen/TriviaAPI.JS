const apiURL = 'https://sat-noroff-api.herokuapp.com'
const apiKey = 'fHinlLXDHB0GKcGgHurrnlIb1wsrQK8qsYBHUJOdAjjV3V4M6cBRf10Ea5atPn7vCA+IFK97AgQRGmoTWQI9MQ=='

export const TriviaAPI = {
    //sets new highscore using call to the noroff trivia api
    async setNewHighScore(id,score) {
        return await fetch(`${apiURL}/trivia/${id}`, {
            method: 'PATCH', // NB: Set method to PATCH
            headers: {
                'X-API-Key': apiKey,
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                // Provide new highScore to add to user with id
                highScore: score
            })
        })
            .then(response => {
                if (!response.ok) {
                    throw new Error('Could not update high score')
                }
                return response.json()
            })
            .then(updatedUser => {
                console.log(updatedUser)
            })
            .catch(error => {
                console.log(error)
            })
    },
     loadNextQuestion(questions,index){
         let questionAlternatives=[]
         if(questions[index].incorrect_answers.length<=0){
             questionAlternatives=['True','False']
         }else {
             questionAlternatives = questions[index].incorrect_answers
             questionAlternatives.push(questions[index].correct_answer)
             questionAlternatives = questionAlternatives
                 .map((value) => ({value, sort: Math.random()}))
                 .sort((a, b) => a.sort - b.sort)
                 .map(({value}) => value)
             return questionAlternatives
         }
     }
}