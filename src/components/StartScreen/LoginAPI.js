const apiURL = 'https://sat-noroff-api.herokuapp.com'
const apiKey = 'fHinlLXDHB0GKcGgHurrnlIb1wsrQK8qsYBHUJOdAjjV3V4M6cBRf10Ea5atPn7vCA+IFK97AgQRGmoTWQI9MQ=='

export const  LoginAPI= {
    //tries to log in user, if not existant it creates a new user
    async login(username){
        // const requestOptions = {
        //     method: "POST",
        //     headers: {"Content-Type": "application/json"},
        //     body: username
        // }

        return await fetch(`${apiURL}/trivia?username=${username}`)
            .then(response => response.json())
            .then(async results => {
                // results will be an array of users that match the username of mega-mind.
                if (results.length > 0) {
                    //console.log("logged in")
                    //console.log(results[0])
                    return results[0]

                } else {
                    return await fetch(`${apiURL}/trivia`, {
                        method: 'POST',
                        headers: {
                            'X-API-Key': apiKey,
                            'Content-Type': 'application/json'
                        },
                        body: JSON.stringify({
                            username: username,
                            highScore: 0
                        })
                    })
                        .then(response => {
                            if (!response.ok) {
                                throw new Error('Could not create new user')
                            }
                            return response.json()
                        })
                        .then(newUser => {
                            // newUser is the new user with an id
                            //console.log("creating new user")
                            //console.log(newUser)
                            return newUser
                        })
                }

            })
            .catch(error => {
                console.log(error)
            })
    },
    ChooseTriviaStats(triviaStats){
        console.log(triviaStats)
    }

}