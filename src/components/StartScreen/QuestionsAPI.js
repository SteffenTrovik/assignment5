
export const QuestionsAPI = {
    // grabs the token for the session for unique questions from opentdb api
    async fetchToken(){
        return await fetch('https://opentdb.com/api_token.php?command=request')
            .then(response => response.json())
            .then(data=>data.token)
    },
    // grabs the avaliable categories from opentdb api
    async fetchCategories(){
        return await fetch('https://opentdb.com/api_category.php')
            .then(response => response.json())
            .then(data=>data.trivia_categories)
    },

    //grabs the amount of questions avaliable from the opentdb api, total or within category
    async fetchQuestionAmountInCategory(Category_Id){
        if(Category_Id){
            return await fetch(`https://opentdb.com/api_count.php?category=${Category_Id}`)
                .then(response => response.json())
                .then(data=>data.data)
                }else{
            return await fetch('https://opentdb.com/api_count_global.php')
                .then(response => response.json())
                .then(data=>data.data)
        }
    },

    //grabs the questions from the opentdb api given the triviaconfig variables
    async fetchQuestions(amount,category,difficulty,QuestionType,token){
        //return fetch(`https://opentdb.com/api.php?amount=10`)
        //let catId=category.id
        //if(catId==undefined) catId =''
        console.log((`https://opentdb.com/api.php?amount=${amount}&category=${category}&difficulty=${difficulty}&type=${QuestionType}&token=${token}`))
        return await fetch(`https://opentdb.com/api.php?amount=${amount}&category=${category}&difficulty=${difficulty}&type=${QuestionType}`)
            .then(response=> response.json())
            .then(response => response.results)
    }
}