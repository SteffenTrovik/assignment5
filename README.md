# Assignment5 Create a Trivia Game using Vue.js

## 1) Set up the development environment
For this app the following tools have been used: Figma, NPM/Node.JS, Vue Cli, Webstorm, browser dev tools and Vue Dev tools, git, Rest API: https://github.com/dewald-els/noroff-assignment-api and Heroku.

## 2) Recommended: Design a component tree
Component tree is designed in figma and is added to the repository.

## 3) Write HTML & CSS as needed
HTML and CSS is basic, guide for css for vue from https://developer.mozilla.org/en-US/docs/Learn/Tools_and_testing/Client-side_JavaScript_frameworks/Vue_styling was followed at first than deviated towoards own customization. Did unfortunately not have time for decent graphics or animations for this project.

## 4) Use the Vue.js framework to build the following screens into your trivia game
Three routes were made showing the three/four screens, start screen, question screen and result screen. start screen was split into two components.

##Appendix requirements:

### 1)  API Configuration (OpenDB and Trivia API)
The API https://opentdb.com/api_config.php with its documentation was used to generate API request based on the triviaconfig input to fetch questions, it was also used to provide some of the alternatives for the triviaconfig input.

The Noroff Trivia API was also deployed on heroku for logging in, creating users and updating highscores.

### 2) Start Screen
The first screen you encounter is a simple login screen, its not password protected so you just specify a username, and the API will either log you in if user exists, or create a new user if not.

Afterwoards it allows you to configure the upcoming trivia, you can select Category,number of question, difficulty and question type.

### 3) Question Screen
The question screen gives you a question at a time with alternatives, clicking a button with an answer takes you to next question until you're done with the questions.

### 4) Result Screen
The last screen displays your score and possible score, and allows you to either replay with different questions, or go back to the trivia config page.

## 5) Submit
The app is deployed on Heroku and can be accessed with the following link:
https://steffenstrivia.herokuapp.com/

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
